FROM hypriot/rpi-alpine-scratch:v3.4

MAINTAINER Jan Burkl <box@5square.de>

RUN apk update \
    && apk upgrade \
    && apk add \
    musl-dev \
    linux-headers \
    python \
    python-dev \
    py-pip \
    gcc \
    && rm -rf /var/cache/apk/*

RUN pip install --upgrade pip && pip install RPi.GPIO
RUN pip install pytest-runner
RUN pip install paho-mqtt

COPY pir.py /pir.py

ENTRYPOINT ["python", "/pir.py"]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/motion-detection" \
      de.5square.homesmarthome.description="Motion detection and mqtt publishing" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
